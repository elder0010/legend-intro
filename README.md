# Reborn Intro

![Reborn Intro](http://www.elder0010.com/uploads/screenshots/reborn-intro.png "Reborn Intro")

A Commodore64 crack intro

### Provided in the repository
- 6502 Assembly Source code ([Kick Assembler] format)
- Executable binaries
- Windows make procedure

### Compiling

You'll need [Java] to compile. To check if you have it use this command:
```
java -version
```
To compile, simply use the included make procedure:
```
make.bat
```

### Todo's
- Translate all comments to English
- Makefile for Linux / OSX

### Credits
- Elder0010 - Code
- Freshness - Code
- Conrad - Music
- Mirage - Graphics

**Free Software, Hell Yeah!**

[Kick Assembler]:http://www.theweb.dk/KickAssembler/Main.php
[Java]:http://java.com/en/download/index.jsp